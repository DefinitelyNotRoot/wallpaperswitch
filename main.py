###WallpaperStyle = 10 (Fill), 6 (Fit), 2 (Stretch), 0 (Tile), 0 (Center)
import ctypes
import winreg
import requests
import os
accessKey="EnterYourAccessKEY"
def downloadWallpaper():
    headers={"Accept-Version": "v1", "Authorization": "Client-ID {}".format(accessKey)}
    try:
        request=requests.get('https://api.unsplash.com/photos/random?orientation=landscape&collections=wallpapers', headers=headers, timeout=5)
        response=request.json()['links']['download']
    except:
        return False
    try:
        image=requests.get(response,headers=headers)
        if image.status_code == 200:
            with open("sample.png", 'wb') as f:
                f.write(image.content)
    except:
        return False

def changeWallpaper():
	try:
		path = os.getcwd() + "\sample.png"
		ctypes.windll.user32.SystemParametersInfoW(20, 0, path , 0)
		return True
	except:
		return False


def registerCentered():
    #changes the registry to center wallpaper
    wallpaperStyle = '0'
    tileWallpaper = '0'
    try:
        ###WallpaperStyle = 10 (Fill), 6 (Fit), 2 (Stretch), 0 (Tile), 0 (Center) 
        WallpaperStyle=0
        desktopKey = _winreg.OpenKey(_winreg.HKEY_CURRENT_USER,'Control Panel\\Desktop',WallpaperStyle,_winreg.KEY_SET_VALUE)
        _winreg.SetValueEx(desktopKey,'WallpaperStyle',0,_winreg.REG_SZ,wallpaperStyle)
        _winreg.SetValueEx(desktopKey,'TileWallpaper',0,_winreg.REG_SZ,tileWallpaper)
        _winreg.CloseKey(desktopKey)
        return True
    except:
        return False


registerCentered()
downloadWallpaper()
changeWallpaper()